public class Yelling {
	
public String scream(String[] names) {
	
	
	
	
	
	if(names.length == 1) {
	    if(names[0] == null) {
	    	return "Nobody is yelling";
		} else if (isNameUppercase(names[0])) {
			return names[0] + " IS YELLING";
		} else {
			return names[0] + " is yelling";
		}
	} else if (names.length == 2) {
		return names[0] + " and " + names[1] + " are yelling";
 	} else {
 		String nameString = generateNameString(names);
 		nameString += " are yelling";
 		
 		return nameString;
 		
 	}


}

private String generateNameString(String[] names) {
	String nameString = "";
	
	for (int index = 0; index < names.length; index++) {
		
		nameString += names[index];
		
		if (index == names.length - 2) {
			nameString += ", and ";
		} else if(index <= names.length - 2) {
			nameString += ", ";
		}
		
	}
	
	return nameString;
}

private boolean isNameUppercase(String name) {
	boolean isUpperCase = true;
	
	for(int index = 0; index < name.length(); index++) {
		if(!Character.isUpperCase(name.charAt(index))) {
			isUpperCase = false;
			break;
		}
	}
	
	return isUpperCase;
}
}