import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class YellingTest {

	@Test
	public void testonepersonyelling()
	{
    Yelling one = new Yelling();
	String result =  one.scream(new String[] {"Peter"});
	assertEquals("Peter is yelling",result);
	}
	
	@Test
	public void testnobodyisyelling()
	{
    Yelling one = new Yelling();
	String result =  one.scream(new String[] {null});
	assertEquals("Nobody is yelling",result);
	}
	
	@Test
	public void uppercaseyelling()
	{
    Yelling one = new Yelling();
	String result =  one.scream(new String[] {"PETER"});
	assertEquals("PETER IS YELLING",result);
	}


	@Test
	public void twopeopleyelling()
	{
    Yelling one = new Yelling();
	String result =  one.scream(new String[] {"Peter","Khadir"});
	assertEquals("Peter and Khadir are yelling",result);
	}


	@Test
	public void morepeopleyelling()
	{
    Yelling one = new Yelling();
	String result =  one.scream(new String[] {"Peter","Khadir","Albert", "Pritesh"});
	assertEquals("Peter, Khadir, Albert, and Pritesh are yelling",result);
	}
	
	@Test
	public void shoutinatlotpeopleyelling()
	{
    Yelling one = new Yelling();
	String result =  one.scream(new String[] {"Peter","EMAD","Albert"});
	assertEquals("Peter and Albert are yelling. SO IS EMAD!",result);
	}
	

	
}
